<?php

namespace Tintch\APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppUserGroups
 *
 * @ORM\Table("usr_by_group")
 * @ORM\Entity
 */
class AppUserGroups
{
    /**
     *
     * @ORM\Id @ORM\ManyToOne(targetEntity="AppGroup")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $group;

    /**
     *
     * @ORM\Id @ORM\ManyToOne(targetEntity="AppUser")
	 * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    public function __construct(AppGroup $group, AppUser $user)
    {
    	$this->group = $group;
    	$this->user = $user;
    }
    /**
     * Set group
     *
     * @param \Tintch\APIBundle\Entity\AppGroup $group
     * @return AppUserGroups
     */
    public function setGroup(\Tintch\APIBundle\Entity\AppGroup $group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \Tintch\APIBundle\Entity\AppGroup 
     */
    public function getGroup()
    {
        return $this->group;
    }


    /**
     * Set user
     *
     * @param \Tintch\APIBundle\Entity\AppUser $user
     * @return AppUserGroups
     */
    public function setUser(\Tintch\APIBundle\Entity\AppUser $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Tintch\APIBundle\Entity\AppUser 
     */
    public function getUser()
    {
        return $this->user;
    }
}
