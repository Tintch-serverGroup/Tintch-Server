<?php

namespace Tintch\APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppEvent
 *
 * @ORM\Table("app_event")
 * @ORM\Entity
 */
class AppEvent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;
    
    /**
     * @var integer
     * 
     * @ORM\ManyToOne(targetEntity="AppUser")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     **/
    private $admin;
    
    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=true, length=20)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=true, length=20)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=true, length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="picture", nullable=true, type="blob")
     */
    private $picture;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin", type="datetime")
     */
    private $begin;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime")
     */
    private $end;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AppEvent
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set long
     *
     * @param float $longitude
     * @return AppEvent
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get long
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return AppEvent
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return AppEvent
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set picture
     *
     * @param string $picture
     * @return AppEvent
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return string 
     */
    public function getPicture()
    {
        return $this->picture;
    }
    /**
     * Set begin
     *
     * @param \DateTime $begin
     * @return AppGroup
     */
    public function setBegin($begin)
    {
    	$this->begin = $begin;
    
    	return $this;
    }
    
    /**
     * Get begin
     *
     * @return \DateTime
     */
    public function getBegin()
    {
    	return $this->begin;
    }
    
    /**
     * Set end
     *
     * @param \DateTime $end
     * @return AppGroup
     */
    public function setEnd($end)
    {
    	$this->end = $end;
    
    	return $this;
    }
    
    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
    	return $this->end;
    }

    /**
     * Set admin
     *
     * @param AppUser $admin
     * @return AppEvent
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
        return $this;
    }

    /**
     * Get admin
     *
     * @return \Tintch\APIBundle\Entity\AppUser 
     */
    public function getAdmin()
    {
        return $this->admin;
    }
}
