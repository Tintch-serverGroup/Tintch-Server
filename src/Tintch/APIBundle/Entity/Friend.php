<?php

namespace Tintch\APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Friend
 *
 * @ORM\Table("friend")
 * @ORM\Entity
 */
class Friend
{
    /**
     *
     * @ORM\Id @ORM\ManyToOne(targetEntity="AppUser")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     *
     * @ORM\Id @ORM\ManyToOne(targetEntity="AppUser")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $friend;

    public function __construct(AppUser $user, AppUser $friend)
    {
    	$this->user = $user;
    	$this->friend = $friend;
    }

    /**
     * Set user
     *
     * @param \Tintch\APIBundle\Entity\AppUser $user
     * @return Friend
     */
    public function setUser(\Tintch\APIBundle\Entity\AppUser $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Tintch\APIBundle\Entity\AppUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set friend
     *
     * @param \Tintch\APIBundle\Entity\AppUser $friend
     * @return Friend
     */
    public function setFriend(\Tintch\APIBundle\Entity\AppUser $friend)
    {
        $this->friend = $friend;

        return $this;
    }

    /**
     * Get friend
     *
     * @return \Tintch\APIBundle\Entity\AppUser 
     */
    public function getFriend()
    {
        return $this->friend;
    }
}
