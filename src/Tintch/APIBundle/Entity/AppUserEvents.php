<?php

namespace Tintch\APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppUserEvents
 *
 * @ORM\Table("usr_by_event")
 * @ORM\Entity
 */
class AppUserEvents
{
    /**
     *
     * @ORM\Id @ORM\ManyToOne(targetEntity="AppEvent")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $event;

    /**
     *
     * @ORM\Id @ORM\ManyToOne(targetEntity="AppUser")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    public function __construct(AppEvent $event, AppUser $user)
    {
    	$this->event = $event;
    	$this->user = $user;
    }
    /**
     * Set event
     *
     * @param AppEvent $event
     * @return AppUserEvents
     */
    public function setEvent(\Tintch\APIBundle\Entity\AppEvent $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return AppEvent 
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set user
     *
     * @param AppUser $user
     * @return AppUserEvents
     */
    public function setUser(\Tintch\APIBundle\Entity\AppUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return AppUser 
     */
    public function getUser()
    {
        return $this->user;
    }
}
