<?php

namespace Tintch\APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppUser
 *
 * @ORM\Table("app_user")
 * @ORM\Entity
 */
class AppUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=50)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", unique=true, length=60)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=50)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="profilPicture", nullable=true, type="blob")
     */
    private $profilPicture;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="index_geolocation", type="integer")
     */
    private $index_geolocation = 0;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="allow_geolocation", type="boolean", options={"default" = true})
     */
    private $allow_geolocation;
    
    public function __construct(){
    	$this->allow_geolocation = true;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return AppUser
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return AppUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return AppUser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set profilPicture
     *
     * @param string $profilPicture
     * @return AppUser
     */
    public function setProfilPicture($profilPicture)
    {
        $this->profilPicture = $profilPicture;

        return $this;
    }

    /**
     * Get profilPicture
     *
     * @return string 
     */
    public function getProfilPicture()
    {
        return $this->profilPicture;
    }
    
    /**
     * Set index_geolocation
     *
     * @param integer $indexGeolocation
     * @return AppUser
     */
    public function setIndexGeolocation($indexGeolocation)
    {
        $this->index_geolocation = $indexGeolocation;

        return $this;
    }

    /**
     * Get index_geolocation
     *
     * @return integer 
     */
    public function getIndexGeolocation()
    {
        return $this->index_geolocation;
    }
    
    /**
     * Set allow_geolocation
     *
     * @param boolean $allow_geolocation
     * @return AppUser
     */
    public function setAllowGeolocation($allow_geolocation)
    {
        $this->allow_geolocation = $allow_geolocation;

        return $this;
    }

    /**
     * Get allow_geolocation
     *
     * @return integer 
     */
    public function getAllowGeolocation()
    {
        return $this->allow_geolocation;
    }
}
