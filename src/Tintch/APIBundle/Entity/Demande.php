<?php

namespace Tintch\APIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Friend
 *
 * @ORM\Table("demande")
 * @ORM\Entity
 */
class Demande
{
    /**
     *
     * @ORM\Id @ORM\ManyToOne(targetEntity="AppUser")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     *
     * @ORM\Id @ORM\ManyToOne(targetEntity="AppUser")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $friend;

    /**
     * 
     * @ORM\Column(name="status_demande", type="boolean")
     */
    private $status;
    //Si le status est a false c'est que la demande a ete refusee 
    //Si le status est a true c est que la demande est en attente
    
    public function __construct(AppUser $user, AppUser $friend, $status = TRUE)
    {
    	$this->user = $user;
    	$this->friend = $friend;
    	$this->status = $status;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Demande
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set user
     *
     * @param \Tintch\APIBundle\Entity\AppUser $user
     * @return Demande
     */
    public function setUser(\Tintch\APIBundle\Entity\AppUser $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Tintch\APIBundle\Entity\AppUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set friend
     *
     * @param \Tintch\APIBundle\Entity\AppUser $friend
     * @return Demande
     */
    public function setFriend(\Tintch\APIBundle\Entity\AppUser $friend)
    {
        $this->friend = $friend;

        return $this;
    }

    /**
     * Get friend
     *
     * @return \Tintch\APIBundle\Entity\AppUser 
     */
    public function getFriend()
    {
        return $this->friend;
    }
}
