<?php

namespace Tintch\APIBundle\Controller;

use Tintch\APIBundle\Entity\AppUser;
use Tintch\APIBundle\Entity\GeolocUser;

use Tintch\APIBundle\Classes\GeolocalisationManager;

use Tintch\APIBundle\Form\GeolocUserCreateType;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;

use Tintch\APIBundle\Classes\ProfilPictureManager;

/**
 * AppUser controller.
 * @RouteResource("Geolocalisation")
 */
class GeolocalisationRESTController extends VoryxController
{
	use TraitSessionManager;
    /**
     * Get a GeolocUser entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getLastTwentyAction(AppUser $entity)
    {
    	try {
    		$session = self::checkSession();
    		if($session->getIdUser() != $entity){
	    		if($entity->getAllowGeolocation() == false){
					return FOSView::create(array('code'=>Codes::HTTP_FORBIDDEN,'message'=>'this user dont allow geolocation'), Codes::HTTP_FORBIDDEN);
	    		}
    		}
    	}
  		catch (\Exception $e){
           	throw $this->createAccessDeniedException();
  		}
  		try {
	  		$gm = new GeolocalisationManager($this->getDoctrine()->getManager(), $entity);
	  		$lastTwentyLocation = $gm->getLastTwentyLocation();

	  		$response = new Response();
	  		$response->setContent($lastTwentyLocation);
	  		return $response;
  		} catch (\Exception $e){
			return FOSView::create(array('code'=>Codes::HTTP_INTERNAL_SERVER_ERROR,'message'=>$e->getMessage()), Codes::HTTP_INTERNAL_SERVER_ERROR);
		}
    }
    /**
     * Get a GeolocUser entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getLastTenAction(AppUser $entity)
    {
    	try {
    		$session = self::checkSession();
    		if($session->getIdUser() != $entity){
	    		if($entity->getAllowGeolocation() == false){
					return FOSView::create(array('code'=>Codes::HTTP_FORBIDDEN,'message'=>'this user dont allow geolocation'), Codes::HTTP_FORBIDDEN);
	    		}
    		}
    	}
  		catch (\Exception $e){
           	throw $this->createAccessDeniedException();
  		}
  		try {
	  		$gm = new GeolocalisationManager($this->getDoctrine()->getManager(), $entity);
	  		$lastTenLocation = $gm->getLastTenLocation();

	  		$response = new Response();
	  		$response->setContent($lastTenLocation);
	  		return $response;
  		} catch (\Exception $e){
			return FOSView::create(array('code'=>Codes::HTTP_INTERNAL_SERVER_ERROR,'message'=>$e->getMessage()), Codes::HTTP_INTERNAL_SERVER_ERROR);
		}
    }
    /**
     * Get a GeolocUser entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getLastFiveAction(AppUser $entity)
    {
    	try {
    		$session = self::checkSession();
    		if($session->getIdUser() != $entity){
	    		if($entity->getAllowGeolocation() == false){
					return FOSView::create(array('code'=>Codes::HTTP_FORBIDDEN,'message'=>'this user dont allow geolocation'), Codes::HTTP_FORBIDDEN);
	    		}
    		}
    	}
  		catch (\Exception $e){
           	throw $this->createAccessDeniedException();
  		}
  		try {
	  		$gm = new GeolocalisationManager($this->getDoctrine()->getManager(), $entity);
	  		$lastFiveLocation = $gm->getLastFiveLocation();

	  		$response = new Response();
	  		$response->setContent($lastFiveLocation);
	  		return $response;
  		} catch (\Exception $e){
			return FOSView::create(array('code'=>Codes::HTTP_INTERNAL_SERVER_ERROR,'message'=>$e->getMessage()), Codes::HTTP_INTERNAL_SERVER_ERROR);
		}
    }
    /**
     * Get a GeolocUser entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getLastAction(AppUser $entity)
    {
    	try {
    		$session = self::checkSession();
    		if($session->getIdUser() != $entity){
	    		if($entity->getAllowGeolocation() == false){
					return FOSView::create(array('code'=>Codes::HTTP_FORBIDDEN,'message'=>'this user dont allow geolocation'), Codes::HTTP_FORBIDDEN);
	    		}
    		}
    	}
  		catch (\Exception $e){
           	throw $this->createAccessDeniedException();
  		}
  		try {
	  		$gm = new GeolocalisationManager($this->getDoctrine()->getManager(), $entity);
	  		$lastLocation = $gm->getLastLocation();

	  		$response = new Response();
	  		$response->setContent($lastLocation);
	  		return $response;
  		} catch (\Exception $e){
			return FOSView::create(array('code'=>Codes::HTTP_INTERNAL_SERVER_ERROR,'message'=>$e->getMessage()), Codes::HTTP_INTERNAL_SERVER_ERROR);
		}
    }
    /**
     * Get all AppUser entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
    	$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
				'SELECT u
		    FROM TintchAPIBundle:AppUser u'
		);
		$result = $query->getResult();
		foreach ($result as $user){
			$gm = new GeolocalisationManager($em, $user);
			if($gm->checkIfNeedInitGeolocTable()){
				$gm->initGeolocationTable();
			}
		}
    }
    /**
     * Create a AppUser entity.
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request)
    {
    	try {
			$session = self::checkSession();
    	}
  		catch (\Exception $e){
           	throw $this->createAccessDeniedException();
  		}
  		if(null != $latitude = $request->request->get('latitude')){
	  		if(null != $longitude = $request->request->get('longitude')){
				$em = $this->getDoctrine()->getManager();
				$gm = new GeolocalisationManager($em, $session->getIdUser());
				try {
					$result = $gm->setLocation($latitude, $longitude);
				} catch (\Exception $e){
					return FOSView::create(array('errors' => $e->getMessage()), Codes::HTTP_INTERNAL_SERVER_ERROR);
				}
				return $result;
	  		} else {
	  			return 'pas bon longitude';
	  		}
  		} else {
	  		return 'pas bon latitude';
	  	}
		return FOSView::create(array('errors' => 'marche pas'), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
    /**
     * Update a AppUser entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request, AppUser $entity)
    {
    	try {
    		self::checkSession($entity);
    	}
  		catch (\Exception $e){
           	throw $this->createAccessDeniedException();
  		}
    }
    /**
     * Partial Update to a AppUser entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
*/
    public function patchAction(Request $request, AppUser $entity)
    {
        return $this->putAction($request, $entity);
    }
    /**
     * Delete a AppUser entity.
     *
     * @View(statusCode=204)
     *
     * @param Request $request
     * @param $entity
     * @internal param $id
     *
     * @return Response
     */
    public function deleteAction(Request $request, AppUser $entity)
    {
    }
}
