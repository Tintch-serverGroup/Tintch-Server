<?php

namespace Tintch\APIBundle\Controller;
use \Exception;

trait TraitSessionManager
{
	protected function checkSession($entity = null){
		if(isset($_GET['id_session'])){
			$id_session = $_GET['id_session'];
			$repository = $this->getDoctrine()->getManager()->getRepository('TintchAPIBundle:Session');
			$good_session = $repository->find($id_session);
			if(is_null($good_session)){
				throw new \Exception();
			}
			if($entity != null){
				if($good_session->getIdUser() != $entity){
					throw new \Exception();
				}
			}
			return $good_session;
		}else{
			throw new \Exception();
		}
	}
}