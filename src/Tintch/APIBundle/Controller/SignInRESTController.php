<?php

namespace Tintch\APIBundle\Controller;

use Tintch\APIBundle\Entity\AppUser;
use Tintch\APIBundle\Form\AppUserType;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;
use Tintch\APIBundle\Form\SingInType;
use Tintch\APIBundle\Entity\Session;

/**
 * AppUser controller.
 * @RouteResource("SignIn")
 */
class SignInRESTController extends VoryxController
{
    /**
     * Get a AppUser entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction()
    {
    }
    /**
     * Get all AppUser entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
	}
    /**
     * SingIn for an user.
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request)
    {
        $entity = new AppUser();
        $form = $this->createForm(new SingInType(), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);

        if ($form->isValid()) {
        	
            $repository = $this->getDoctrine()->getManager()->getRepository('TintchAPIBundle:AppUser');
            $user = $repository->findOneBy(array('email' => $entity->getEmail(), 'password' => $entity->getPassword()));
            if(false == $user){
            	throw $this->createAccessDeniedException();
            }
            
            $session = new Session();
            $session->setIdUser($user);
            
            $starttime = new \DateTime('NOW');
            $starttime->format(\DateTime::W3C);
    		
            $session->setStartTime($starttime);
            $numMinutes = 30;

            $endtime = clone $starttime;
            $endtime->modify ("+{$numMinutes} minutes");
            
            
            $session->setEndTime($endtime);

            $em = $this->getDoctrine()->getManager();
		    $em->persist($session);
		    $em->flush();
            			
			return $session->getId();
        }

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
    /**
     * Update a AppUser entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request, AppUser $entity)
    {
	}
    /**
     * Partial Update to a AppUser entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
*/
    public function patchAction(Request $request, AppUser $entity)
    {
    }
    /**
     * Delete a AppUser entity.
     *
     * @View(statusCode=204)
     *
     * @param Request $request
     * @param $entity
     * @internal param $id
     *
     * @return Response
     */
    public function deleteAction(Request $request, AppUser $entity)
    {
	}
}
