<?php

namespace Tintch\APIBundle\Controller;

use Tintch\APIBundle\Entity\AppUserGroups;
use Tintch\APIBundle\Entity\AppGroup;
use Tintch\APIBundle\Entity\AppUser;

use Tintch\APIBundle\Form\AppUserGroupsType;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;

/**
 * AppUserGroups controller.
 * @RouteResource("UserByGroup")
 */
class AppUserGroupsRESTController extends VoryxController
{
	use TraitSessionManager;
    /**
     * Get a AppUserGroups entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction(AppUserGroups $entity)
    {
    }
    /**
     * Get all AppUserGroups entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
    }
    /**
     * Create a AppUserGroups entity.
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
        $id_group = $request->request->get('group');
        $id_user = $request->request->get('user');
		if( null !== $id_group && null !== $id_user ){
	        $em = $this->getDoctrine()->getManager();
	
	        $group = $em->getRepository('TintchAPIBundle:AppGroup')->find($id_group);
	
	        if($session->getIdUser() != $group->getAdmin()){
				return FOSView::create(array('code'=>Codes::HTTP_FORBIDDEN,'message'=>'Forbidden'), Codes::HTTP_FORBIDDEN);
	        }        
	        $user = $em->getRepository('TintchAPIBundle:AppUser')->find($id_user);
	
	        if($group != false && $user != false){
	        	$entity = new AppUserGroups($group,$user);
	        	
	        	$em->persist($entity);
				$em->flush();
	
				return FOSView::create(array('code'=>Codes::HTTP_OK,'message'=>'user add to group'), Codes::HTTP_OK);
	        }
		}
        return FOSView::create(array('errors' => 'user or group not correct'), Codes::HTTP_BAD_REQUEST);
    }
    /**
     * Update a AppUserGroups entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request, AppUserGroups $entity)
    {
    }
    /**
     * Partial Update to a AppUserGroups entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
*/
    public function patchAction(Request $request, AppUserGroups $entity)
    {
    }
    /**
     * Delete a AppUserGroups entity.
     *
     * @return Response
     */
    public function deleteAction(Request $request)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
        $id_group = $request->request->get('group');
        $id_user = $request->request->get('user');
		if(null !== $id_group && null !== $id_user){
	        $em = $this->getDoctrine()->getManager();
			$usergroup = $em->getRepository('TintchAPIBundle:AppUserGroups')->findOneBy(array('group' => $id_group, 'user' => $id_user));
			if($usergroup != false){
				//tester si utilisateur � supprimer est lui meme.
		        if($session->getIdUser() != $usergroup->getGroup()->getAdmin()){
					return FOSView::create(array('code'=>Codes::HTTP_FORBIDDEN,'message'=>'Forbidden'), Codes::HTTP_FORBIDDEN);
		        }
				try {
			        $em->remove($usergroup);
			        $em->flush();
					return FOSView::create(array('code'=>Codes::HTTP_OK,'message'=>'user remove to group'), Codes::HTTP_OK);
				}catch (\Exception $e){
					return FOSView::create(array('code'=>Codes::HTTP_INTERNAL_SERVER_ERROR,'message'=> $e->getMessage()), Codes::HTTP_INTERNAL_SERVER_ERROR);
				}
		    }
		}
        return FOSView::create(array('errors' => 'user or group not correct'), Codes::HTTP_BAD_REQUEST);
    }
}
