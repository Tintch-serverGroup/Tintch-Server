<?php

namespace Tintch\APIBundle\Controller;

use Tintch\APIBundle\Entity\AppUser;
use Tintch\APIBundle\Entity\Friend;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;

/**
 * @RouteResource("Friend")
 */
class FriendRESTController extends VoryxController
{
	use TraitSessionManager;
	/**
	 * Get a Friend entity
	 *
	 * @View(serializerEnableMaxDepthChecks=true)
	 *
	 * @return Response
	 *
	 */
	public function getAction(AppUser $entity)
	{
	}
	/**
	 * Get all AppUser entities.
	 *
	 * @View(serializerEnableMaxDepthChecks=true)
	 *
	 * @return Response
	 *
	 */
	public function cgetAction()
	{
		try {
			$session = self::checkSession();
		}
		catch (\Exception $e){
			throw $this->createAccessDeniedException();
		}
		$em = $this->getDoctrine()->getManager();
		try {
			$users = $em->getRepository('TintchAPIBundle:Friend')->findBy(array('user'=>$session->getIdUser()));
			if($users != false){
				$tab = array();
				foreach ($users as $u){
					$user = $u->getFriend();
					if($user->getProfilPicture() != null){
						$urlProfilPicture = stream_get_contents($user->getProfilPicture());
						$base64 = base64_encode(file_get_contents($urlProfilPicture));
						$tab[] = array('id'=>$user->getId(), 'username'=>$user->getUsername(),basename($urlProfilPicture)=>$base64);
					}else{
						$tab[] = array('id'=>$user->getId(), 'username'=>$user->getUsername());
					}
				}
		    	$json = json_encode($tab, JSON_UNESCAPED_SLASHES);
		    	 
		    	$response = new Response();
		    	$response->setContent($json);
				return $response;
			}else {
				return FOSView::create(array('code'=>Codes::HTTP_NO_CONTENT,'message'=>'No friend found'), Codes::HTTP_NO_CONTENT);
			}
		}catch (\Exception $e){
			return FOSView::create(array('code'=>Codes::HTTP_INTERNAL_SERVER_ERROR,'message'=>$e->getMessage()), Codes::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
	/**
	 * Create a AppUser entity.
	 *
	 * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
	 *
	 * @param Request $request
	 *
	 * @return Response
	 *
	 */
	public function postAction(Request $request)
	{
	}
	/**
	 * Update a AppUser entity.
	 *
	 * @View(serializerEnableMaxDepthChecks=true)
	 *
	 * @param Request $request
	 * @param $entity
	 *
	 * @return Response
	 */
	public function putAction(Request $request, AppUser $entity)
	{
	}
	/**
	 * Partial Update to a AppUser entity.
	 *
	 * @View(serializerEnableMaxDepthChecks=true)
	 *
	 * @param Request $request
	 * @param $entity
	 *
	 * @return Response
	 */
	public function patchAction(Request $request, AppUser $entity)
	{
	}
	/**
	 * Delete a Friend entity.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function deleteAction(Request $request)
	{
		try {
			$session = self::checkSession();
		}
		catch (\Exception $e){
			throw $this->createAccessDeniedException();
		}
		$em = $this->getDoctrine()->getManager();
		if(null != $friend_id = $request->request->get('friend')){
			$friendship1 = $em->getRepository('TintchAPIBundle:Friend')->findOneBy(array('user'=>$session->getIdUser(), 'friend'=>$friend_id));
			$friendship2 = $em->getRepository('TintchAPIBundle:Friend')->findOneBy(array('user'=>$friend_id, 'friend'=>$session->getIdUser()));
			if($friendship1 ==! null && $friendship2 ==! null){
				try {
					$em->remove($friendship1);
					$em->remove($friendship2);
					$em->flush();
					return FOSView::create(array('code'=>Codes::HTTP_OK,'message'=>'Friendship deleted'), Codes::HTTP_OK);
				}catch (\Exception $e){
					return FOSView::create(array('code'=>$e->getCode(),'message'=>$e->getMessage()), $e->getCode());
				}
			}else{
				return FOSView::create(array('code'=>Codes::HTTP_NOT_FOUND,'message'=>'Friendship not found'), Codes::HTTP_NOT_FOUND);
			}
		}else{
			return FOSView::create(array('code'=>Codes::HTTP_BAD_REQUEST,'message'=>'Please define parameter friend'), Codes::HTTP_BAD_REQUEST);
		}
	}
}
