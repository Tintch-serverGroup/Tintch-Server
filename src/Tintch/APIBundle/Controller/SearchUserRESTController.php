<?php

namespace Tintch\APIBundle\Controller;

use Tintch\APIBundle\Entity\AppUser;
use Tintch\APIBundle\Form\AppUserType;
use Tintch\APIBundle\Form\AppUserPutType;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;

/**
 * AppUser controller.
 * @RouteResource("SearchUser")
 */
class SearchUserRESTController extends VoryxController
{
	use TraitSessionManager;
	/**
	 * Get a AppUser entity
	 *
	 * @View(serializerEnableMaxDepthChecks=true)
	 *
	 * @return Response
	 *
	 */
	public function getAction(AppUser $entity)
	{
	}
	/**
	 * Get all AppUser entities.
	 *
	 * @View(serializerEnableMaxDepthChecks=true)
	 *
	 * @return Response
	 *
	 */
	public function cgetAction()
	{
		try {
			self::checkSession();
		}
		catch (\Exception $e){
			throw $this->createAccessDeniedException();
		}
		if(isset($_GET['search']) && strlen($_GET['search'])>2){
			$search = $_GET['search'];
			$em = $this->getDoctrine()->getManager();
			try {
				$query = $em->createQuery("SELECT u.id, u.username, u.profilPicture 
						FROM TintchAPIBundle:AppUser u 
						WHERE u.username LIKE :search
						OR u.email LIKE :search");
				$query->setParameter('search', "%$search%");
				$users = $query->getResult();
				if($users != false){
					$tab = array();
					foreach ($users as $user){
						if($user['profilPicture'] != null){
							$urlProfilPicture = stream_get_contents($user['profilPicture']);
							$base64 = base64_encode(file_get_contents($urlProfilPicture));
							$tab[] = array('id'=>$user['id'], 'username'=>$user['username'],basename($urlProfilPicture)=>$base64);
						}else{
							$tab[] = array('id'=>$user['id'], 'username'=>$user['username']);
						}
					}
			    	$json = json_encode($tab, JSON_UNESCAPED_SLASHES);
			    	 
			    	$response = new Response();
			    	$response->setContent($json);
					return $response;
				}else {
					return FOSView::create(array('code'=>Codes::HTTP_OK,'message'=>'No users found'), Codes::HTTP_OK);
				}
			}catch (\Exception $e){
				return FOSView::create(array('code'=>Codes::HTTP_INTERNAL_SERVER_ERROR,'message'=>$e->getMessage()), Codes::HTTP_INTERNAL_SERVER_ERROR);
			}
		}else{
			return FOSView::create(array('code'=>Codes::HTTP_BAD_REQUEST,'message'=>'Bad request, please define search=... or enter more caractere'), Codes::HTTP_BAD_REQUEST);
		}
	}
	/**
	 * Create a AppUser entity.
	 *
	 * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
	 *
	 * @param Request $request
	 *
	 * @return Response
	 *
	 */
	public function postAction(Request $request)
	{
	}
	/**
	 * Update a AppUser entity.
	 *
	 * @View(serializerEnableMaxDepthChecks=true)
	 *
	 * @param Request $request
	 * @param $entity
	 *
	 * @return Response
	 */
	public function putAction(Request $request, AppUser $entity)
	{
	}
	/**
	 * Partial Update to a AppUser entity.
	 *
	 * @View(serializerEnableMaxDepthChecks=true)
	 *
	 * @param Request $request
	 * @param $entity
	 *
	 * @return Response
	 */
	public function patchAction(Request $request, AppUser $entity)
	{
	}
	/**
	 * Delete a AppUser entity.
	 *
	 * @View(statusCode=204)
	 *
	 * @param Request $request
	 * @param $entity
	 * @internal param $id
	 *
	 * @return Response
	 */
	public function deleteAction(Request $request, AppUser $entity)
	{
	}
}
