<?php

namespace Tintch\APIBundle\Controller;

use Tintch\APIBundle\Entity\AppEvent;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;

use Tintch\APIBundle\Classes\ProfilPictureManager;

/**
 * @RouteResource("EventPicture")
 */
class EventPictureRESTController extends VoryxController
{
	use TraitSessionManager;
    /**
     * Get a AppEvent entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getSmallAction(AppEvent $entity)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	if($entity->getProfilPicture() != null){
    		$urlProfilPicture = stream_get_contents($entity->getProfilPicture());
    		$base64 = base64_encode(file_get_contents($urlProfilPicture));
    	}else{
    		$base64 = null;
    	}
    	$array = array(basename($urlProfilPicture)=>$base64);
    	$json = json_encode($array, JSON_UNESCAPED_SLASHES);
    	 
    	$response = new Response();
    	$response->setContent($json);
    	
    	return $response;
    }
    /**
     * Get a AppUser entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAllAction(AppUser $entity)
    {
    
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
        try {
        	$pictue_manager = new ProfilPictureManager($entity->getId());
    		$tab = $pictue_manager->getAllProfilPictureIcon();
    		
        	$json = json_encode($tab, JSON_UNESCAPED_SLASHES);
        	
        	$response = new Response();
        	$response->setContent($json);
        	
			return $response;
        	
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Get a AppUser entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getOriginalAction(AppUser $entity)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
        try {
        	if(isset($_GET['profilPicture'])){
        		$picture = $_GET['profilPicture'];
        		$pm = new ProfilPictureManager($entity->getId());
        		$tab = $pm->getOriginalPicture($picture);
        	
	        	$json = json_encode($tab, JSON_UNESCAPED_SLASHES);
	        	
	        	$response = new Response();
	        	$response->setContent($json);
	        	
				return $response;
        	}else{
				return FOSView::create(array('code'=>Codes::HTTP_BAD_REQUEST,'message'=>'Bad request, please define profilPicture=...'), Codes::HTTP_BAD_REQUEST);
        	}
        } catch (\Exception $e) {
            return FOSView::create(array('code'=>$e->getCode(),'message'=>$e->getMessage()), $e->getCode());
        }
    }
    /**
     * Get all AppUser entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
    }
    /**
     * Create a AppUser entity.
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	try {
    		if(isset($_FILES["profilPicture"])){
    			$file = $_FILES["profilPicture"];
    		}else{
    		    return FOSView::create("No file send", Codes::HTTP_INTERNAL_SERVER_ERROR);
        	}
        	$pictue_manager = new ProfilPictureManager($session->getIdUser()->getId(), $file);
    		$target_file = $pictue_manager->uploadPicture();
    		
	    	$em = $this->getDoctrine()->getManager();
	    	$entity = $em->getRepository('TintchAPIBundle:AppUser')->find($session->getIdUser());
	    	
	    	$entity->setProfilPicture($target_file);
	    	$em->flush();
        } catch (\Exception $e) {
            return FOSView::create(array('code'=>$e->getCode(),'message'=>$e->getMessage()), $e->getCode());
        }
    	return FOSView::create(array('code'=>Codes::HTTP_CREATED,'message'=>'Profil picture updloaded'), Codes::HTTP_CREATED);
    }
    /**
     * Update a AppUser entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request, AppUser $entity)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	if($session->getIdUser()->getId() != $entity->getId()){
        	throw $this->createAccessDeniedException();
        }
        try {
        	if(null !== $request->request->get('profilPicture')){
        		$filename = $request->request->get('profilPicture');
        	
				$target_file = dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR .'pictures'.DIRECTORY_SEPARATOR.
					'users'.DIRECTORY_SEPARATOR.$entity->getId().DIRECTORY_SEPARATOR.'icon'.DIRECTORY_SEPARATOR.$filename;
				if(is_file($target_file)){
					$entity->setProfilPicture($target_file);
		    		$em = $this->getDoctrine()->getManager();
		    		$em->persist($entity);
		    		$em->flush();
		    		
					return FOSView::create(array('code'=>Codes::HTTP_OK,'message'=>'Profil picture changed'), Codes::HTTP_OK);
				}else{
					return FOSView::create(array('code'=>Codes::HTTP_NOT_FOUND,'message'=>'File not found'), Codes::HTTP_NOT_FOUND);
				}
        	}else{
				return FOSView::create(array('code'=>Codes::HTTP_BAD_REQUEST,'message'=>'Bad request, please set the parametre profilPicture in your resquest'), Codes::HTTP_BAD_REQUEST);
        	}
        } catch (\Exception $e) {
            return FOSView::create(array('code'=>$e->getCode(),'message'=>$e->getMessage()), $e->getCode());
        }
    }
    /**
     * Partial Update to a AppUser entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function patchAction(Request $request, AppUser $entity)
    {
        return $this->putAction($request, $entity);
    }
    /**
     * Delete a AppUser entity.
     *
     *
     * @param Request $request
     * @param $entity
     * @internal param $id
     *
     * @return Response
     */
    public function deleteAction(Request $request, AppUser $entity)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	if($session->getIdUser()->getId() != $entity->getId()){
        	throw $this->createAccessDeniedException();
        }
        try {
        	if(null !== $request->request->get('profilPicture')){
        		$filename = $request->request->get('profilPicture');
        	
	        	$pictue_manager = new ProfilPictureManager($session->getIdUser()->getId());
	    		$pictue_manager->deletePicture($filename);

	    		$urlProfilPicture = stream_get_contents($entity->getProfilPicture());
	    		if(basename($urlProfilPicture) == $filename){
	    			$entity->setProfilPicture(null);
	    		}

	    		$em = $this->getDoctrine()->getManager();
	    		$em->persist($entity);
	    		$em->flush();
	    		
				return FOSView::create(array('code'=>Codes::HTTP_OK,'message'=>'Picture deleted'), Codes::HTTP_OK);
        	}else{
				return FOSView::create(array('code'=>Codes::HTTP_BAD_REQUEST,'message'=>'Bad request, please set the parametre profilPicture in your resquest'), Codes::HTTP_BAD_REQUEST);
        	}
        } catch (\Exception $e) {
            return FOSView::create(array('code'=>$e->getCode(),'message'=>$e->getMessage()), $e->getCode());
        }
    }
}