<?php

namespace Tintch\APIBundle\Controller;

use Tintch\APIBundle\Entity\AppGroup;
use Tintch\APIBundle\Entity\AppUser;
use Tintch\APIBundle\Form\AppGroupCreateType;
use Tintch\APIBundle\Form\AppGroupPutType;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;
use Tintch\APIBundle\Entity\AppUserGroups;

/**
 * AppGroup controller.
 * @RouteResource("Group")
 */
class AppGroupRESTController extends VoryxController
{
	use TraitSessionManager;
    /**
     * Get a AppGroup entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction(AppGroup $entity)
    {    	
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	$em = $this->getDoctrine()->getManager();
    	$result = $em->getRepository('TintchAPIBundle:AppUserGroups')->findBy(array('group'=>$entity, 'user'=>$session->getIdUser()));
    	if(!empty($result)){
			$query = $em->createQuery(
			    'SELECT u
			    FROM TintchAPIBundle:AppUser u
				LEFT JOIN TintchAPIBundle:AppUserGroups ubg
				WITH u.id = ubg.user
			    WHERE ubg.group = :group'
			)->setParameter('group', $entity);
			$users = $query->getResult();
			$users_tab = array();
			foreach ($users as $user){
				$users_tab[] = array('id' => $user->getId(), 'username' => $user->getUsername());
			}
			$tab = array();
			$tab[] = array(				
            		'id' => $entity->getId(),
            		'name' => $entity->getName(),
            		'admin' => array('id'=>$entity->getAdmin()->getId(), 'username'=>$entity->getAdmin()->getUsername()),
            		'description' => $entity->getDescription(),
            		'users' => $users_tab,
			);
			
			$tab = json_encode($tab, JSON_UNESCAPED_SLASHES);
			
			$response = new Response();
			$response->setContent($tab);
			return $response;
    	}else {
    		throw $this->createAccessDeniedException();
    	}
    	
    }
    /**
     * Get all AppGroup entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
        try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	   	
    	$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
		    'SELECT g
		    FROM TintchAPIBundle:AppGroup g
			LEFT JOIN TintchAPIBundle:AppUserGroups ubg
			WITH g.id = ubg.group
		    WHERE ubg.user = :user'
		)->setParameter('user', $session->getIdUser());
		
		$entities = $query->getResult();
		
		$tab = array();
		foreach ($entities as $entity){
			$tab[] = array(
					'id' => $entity->getId(),
					'name' => $entity->getName(),
					'admin' => array('id'=>$entity->getAdmin()->getId(), 'username'=>$entity->getAdmin()->getUsername()),
			);
		}
		$tab = json_encode($tab, JSON_UNESCAPED_SLASHES);

		$response = new Response();
		$response->setContent($tab);
		
		return $response;
    }
    /**
     * Create a AppGroup entity.
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	
        $entity = new AppGroup();
        $form = $this->createForm(new AppGroupCreateType(), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);

        if ($form->isValid()) {
        	$entity->setAdmin($session->getIdUser());

        	$em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $userGroup = new AppUserGroups($entity, $entity->getAdmin());
            $em->persist($userGroup);
            
            $em->flush();
            
            $tab = array();
            $tab[] = array(
            		'id' => $entity->getId(),
            		'name' => $entity->getName(),
            		'admin' => array('id'=>$entity->getAdmin()->getId(), 'username'=>$entity->getAdmin()->getUsername()),
            		'description' => $entity->getDescription(),
            		'user' => array('id'=>$entity->getAdmin()->getId(), 'username'=>$entity->getAdmin()->getUsername()),
            );
            $tab = json_encode($tab, JSON_UNESCAPED_SLASHES);
            
            $response = new Response();
            $response->setContent($tab);

            return $response;
        }

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
    /**
     * Update a AppGroup entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request, AppGroup $entity)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	if($session->getIdUser() != $entity->getAdmin()){
    		throw $this->createAccessDeniedException();
    	}
    	
        try {
            $em = $this->getDoctrine()->getManager();
            $request->setMethod('PATCH'); //Treat all PUTs as PATCH
            $form = $this->createForm(new AppGroupPutType(), $entity, array("method" => $request->getMethod()));
            $this->removeExtraFields($request, $form);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->flush();

                return $entity;
            }

            return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Partial Update to a AppGroup entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
*/
    public function patchAction(Request $request, AppGroup $entity)
    {
        return $this->putAction($request, $entity);
    }
    /**
     * Delete a AppGroup entity.
     *
     * @View(statusCode=204)
     *
     * @param Request $request
     * @param $entity
     * @internal param $id
     *
     * @return Response
     */
    public function deleteAction(Request $request, AppGroup $entity)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	if($session->getIdUser() != $entity->getAdmin()){
    		throw $this->createAccessDeniedException();
    	}
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            return null;
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
