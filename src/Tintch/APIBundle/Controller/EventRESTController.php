<?php

namespace Tintch\APIBundle\Controller;

use Tintch\APIBundle\Entity\AppEvent;
use Tintch\APIBundle\Form\CreateEventType;
use Tintch\APIBundle\Entity\AppUserEvents;
use Tintch\APIBundle\Form\EventPutType;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;
use Tintch\APIBundle\TintchAPIBundle;

/**
 * AppEvent controller.
 * @RouteResource("Event")
 */
class EventRESTController extends VoryxController
{
	use TraitSessionManager;
    /**
     * Get a AppEvent entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction(AppEvent $entity)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	$em = $this->getDoctrine()->getManager();
    	$result = $em->getRepository('TintchAPIBundle:AppUserEvents')->findBy(array('event'=>$entity, 'user'=>$session->getIdUser()));

    	if(!empty($result)){
        	return $entity;
    	}else {
    		throw $this->createAccessDeniedException();
    	}
    }
    /**
     * Get all AppEvent entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	   	
    	$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
		    'SELECT e
		    FROM TintchAPIBundle:AppEvent e
			LEFT JOIN TintchAPIBundle:AppUserEvents ube
			WITH e.id = ube.event
		    WHERE ube.user = :user'
		)->setParameter('user', $session->getIdUser());

		$entities = $query->getResult();
		$array = array();
		foreach ($entities as $entity){
			$array["id"] = $entity->getId();
			$array["name"] = $entity->getName();
			$array["admin"]["id"] = $entity->getAdmin()->getId();
			$array["admin"]["username"] = $entity->getAdmin()->getUsername();
	    	if($entity->getAdmin()->getProfilPicture() != null){
		    	$urlProfilPicture = stream_get_contents($entity->getAdmin()->getProfilPicture());
				$array["profilPicture"] = base64_encode(file_get_contents($urlProfilPicture));
	    	}
			$array["begin"] = $entity->getBegin();
			$array["end"] = $entity->getEnd();
		}

		$json = json_encode($array, JSON_UNESCAPED_SLASHES);
		 
		$response = new Response();
		$response->setContent($json);
		
		return $response;
    }
    /**
     * Create a AppEvent entity.
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
        $entity = new AppEvent();
                
        $begin = new \DateTime($request->request->get('begin'));
        $begin->format(\DateTime::W3C);
        $end = new \DateTime($request->request->get('end'));
        $end->format(\DateTime::W3C);
        
        $form = $this->createForm(new CreateEventType(), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);

        if ($form->isValid()) {
        	$entity->setAdmin($session->getIdUser());
        	$entity->setBegin($begin);
        	$entity->setEnd($end);
        	
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $userEvent = new AppUserEvents($entity, $entity->getAdmin());
            $em->persist($userEvent);
            
            $em->flush();
            
            return $entity;
        }

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
    /**
     * Update a AppEvent entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request, AppEvent $entity)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	if($session->getIdUser() != $entity->getAdmin()){
    		throw $this->createAccessDeniedException();
    	}
        try {
            $em = $this->getDoctrine()->getManager();
            $request->setMethod('PATCH'); //Treat all PUTs as PATCH
            $form = $this->createForm(new EventPutType(), $entity, array("method" => $request->getMethod()));
            $this->removeExtraFields($request, $form);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->flush();

                return $entity;
            }

            return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Partial Update to a AppEvent entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
*/
    public function patchAction(Request $request, AppEvent $entity)
    {
        return $this->putAction($request, $entity);
    }
    /**
     * Delete a AppEvent entity.
     *
     * @View(statusCode=204)
     *
     * @param Request $request
     * @param $entity
     * @internal param $id
     *
     * @return Response
     */
    public function deleteAction(Request $request, AppEvent $entity)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	if($session->getIdUser() != $entity->getAdmin()){
    		throw $this->createAccessDeniedException();
    	}
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            return null;
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
