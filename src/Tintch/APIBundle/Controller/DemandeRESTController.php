<?php

namespace Tintch\APIBundle\Controller;

use Tintch\APIBundle\Entity\Demande;
use Tintch\APIBundle\Entity\Friend;
use Tintch\APIBundle\Entity\AppUser;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;

/**
 * 
 * @RouteResource("friendAsking")
 */
class DemandeRESTController extends VoryxController
{
	use TraitSessionManager;
	/**
	 * Get a Demande entity
	 *
	 * @View(serializerEnableMaxDepthChecks=true)
	 *
	 * @return Response
	 *
	 */
	public function getAction(Demande $entity)
	{
	}
	/**
	 * Get all Demande entities for the user who ask.
	 *
	 * @View(serializerEnableMaxDepthChecks=true)
	 *
	 * @return Response
	 *
	 */
	public function cgetAction()
	{
		try {
			$session = self::checkSession();
		}
		catch (\Exception $e){
			throw $this->createAccessDeniedException();
		}
		try {
			$em = $this->getDoctrine()->getManager();
			$query = $em->createQuery("SELECT u.id, u.username, u.profilPicture
						FROM TintchAPIBundle:Demande d
						LEFT JOIN TintchAPIBundle:AppUser u
						WITH d.user = u.id
						AND d.friend = :user
						AND d.status = TRUE");
			$query->setParameter('user', $session->getIdUser());
			$users = $query->getResult();
			if(null != $users[0]['id']){
				$tab = array();
				foreach ($users as $user){
					if($user['profilPicture'] != null){
						$urlProfilPicture = stream_get_contents($user['profilPicture']);
						$base64 = base64_encode(file_get_contents($urlProfilPicture));
						$tab[] = array('id'=>$user['id'], 'username'=>$user['username'],basename($urlProfilPicture)=>$base64);
					}else{
						$tab[] = array('id'=>$user['id'], 'username'=>$user['username']);
					}
				}
				$json = json_encode($tab, JSON_UNESCAPED_SLASHES);
				 
				$response = new Response();
				$response->setContent($json);
				return $response;
			}else {
				return FOSView::create(array('code'=>Codes::HTTP_NO_CONTENT,'message'=>'No demands found'), Codes::HTTP_NO_CONTENT);
			}
		}catch (\Exception $e){
			return FOSView::create(array('code'=>Codes::HTTP_INTERNAL_SERVER_ERROR,'message'=>$e->getMessage()), Codes::HTTP_INTERNAL_SERVER_ERROR);
		}
		
	}
	/**
	 * Create a Demande entity.
	 *
	 * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
	 *
	 * @param Request $request
	 *
	 * @return Response
	 *
	 */
	public function postAction(Request $request)
	{
		try {
			$session = self::checkSession();
		}
		catch (\Exception $e){
			throw $this->createAccessDeniedException();
		}
		if(null != $friend_id = $request->request->get('friend')){
			$em = $this->getDoctrine()->getManager();
			if(null != $friend = $em->getRepository('TintchAPIBundle:AppUser')->find($friend_id)){
				if( false !== $friendship = $em->getRepository('TintchAPIBundle:Friend')->findOneBy(array('user'=>$session->getIdUser(), 'friend'=>$friend))){
					$demande = new Demande($session->getIdUser(), $friend);
					$em->persist($demande);
					$em->flush();
    				return FOSView::create(array('code'=>Codes::HTTP_CREATED,'message'=>'Demand send to user'), Codes::HTTP_CREATED);
				}else{
    				return FOSView::create(array('code'=>Codes::HTTP_BAD_REQUEST,'message'=>'You are already firend with this user'), Codes::HTTP_BAD_REQUEST);
				}
			}else{
    			return FOSView::create(array('code'=>Codes::HTTP_NOT_FOUND,'message'=>'User not found'), Codes::HTTP_NOT_FOUND);
			}
		}else{
			return FOSView::create(array('code'=>Codes::HTTP_BAD_REQUEST,'message'=>'Please define, parameter friend'), Codes::HTTP_BAD_REQUEST);
		}
	}
	/**
	 * Say yes to a Demande entity.
	 *
	 * @View(serializerEnableMaxDepthChecks=true)
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function putYesAction(Request $request)
	{
		try {
			$session = self::checkSession();
		}
		catch (\Exception $e){
			throw $this->createAccessDeniedException();
		}$em = $this->getDoctrine()->getManager();
		if(null != $friend_id = $request->request->get('friend')){
			if(null != $friend = $em->getRepository('TintchAPIBundle:AppUser')->find($friend_id)){
				if( null !== $demand = $em->getRepository('TintchAPIBundle:Demande')->findOneBy(array('user'=>$friend, 'friend'=>$session->getIdUser()))){
					if($demand->getFriend() ==  $session->getIdUser()){
						$friendship = new Friend($demand->getUser(), $demand->getFriend());
						$friendship2 = new Friend($demand->getFriend(),$demand->getUser());
						try {
							$em = $this->getDoctrine()->getManager();
							$em->persist($friendship);
							$em->persist($friendship2);
							$em->flush();
							$em->remove($demand);
							$em->flush();
							return FOSView::create(array('code'=>Codes::HTTP_CREATED,'message'=>'Firendship created'), Codes::HTTP_CREATED);
						}catch (\Exception $e){
							return FOSView::create(array('code'=>$e->getCode(),'message'=>$e->getMessage()), $e->getCode());
						}
					}else{
						return $this->createAccessDeniedException();
					}
				}else{
					return FOSView::create(array('code'=>Codes::HTTP_NOT_FOUND,'message'=>'This demand does not exist'), Codes::HTTP_NOT_FOUND);
				}
			}else{
				return FOSView::create(array('code'=>Codes::HTTP_NOT_FOUND,'message'=>'Friend user not found'), Codes::HTTP_NOT_FOUND);
			}
		}else{
			return FOSView::create(array('code'=>Codes::HTTP_BAD_REQUEST,'message'=>'Please define parameter friend'), Codes::HTTP_BAD_REQUEST);
		}
	}
	/**
	 * Say no to a Demande entity.
	 *
	 * @View(serializerEnableMaxDepthChecks=true)
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function putNoAction(Request $request)
	{
		try {
			$session = self::checkSession();
		}
		catch (\Exception $e){
			throw $this->createAccessDeniedException();
		}
		$em = $this->getDoctrine()->getManager();
		if(null != $friend_id = $request->request->get('friend')){
			if(null != $friend = $em->getRepository('TintchAPIBundle:AppUser')->find($friend_id)){
				if( null !== $demand = $em->getRepository('TintchAPIBundle:Demande')->findOneBy(array('user'=>$friend, 'friend'=>$session->getIdUser()))){
					if($demand->getFriend() ==  $session->getIdUser()){
						try {
							$demand->setStatus(FALSE);
							$em->persist($demand);
							$em->flush();
							return FOSView::create(array('code'=>Codes::HTTP_OK,'message'=>'Demand rejected'), Codes::HTTP_OK);
						}catch (\Exception $e){
							return FOSView::create(array('code'=>$e->getCode(),'message'=>$e->getMessage()), $e->getCode());
						}
					}else{
						return $this->createAccessDeniedException();
					}
				}else{
					return FOSView::create(array('code'=>Codes::HTTP_NOT_FOUND,'message'=>'This demand does not exist'), Codes::HTTP_NOT_FOUND);
				}
			}else{
				return FOSView::create(array('code'=>Codes::HTTP_NOT_FOUND,'message'=>'Friend user not found'), Codes::HTTP_NOT_FOUND);
			}
		}else{
			return FOSView::create(array('code'=>Codes::HTTP_BAD_REQUEST,'message'=>'Please define parameter friend'), Codes::HTTP_BAD_REQUEST);
		}
	}
	/**
	 * Partial Update to a AppUser entity.
	 *
	 * @View(serializerEnableMaxDepthChecks=true)
	 *
	 * @param Request $request
	 * @param $entity
	 *
	 * @return Response
	 */
	public function patchAction(Request $request, Demande $entity)
	{
	}
	/**
	 * Delete a Demande entity.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function deleteAction(Request $request)
	{
		try {
			$session = self::checkSession();
		}
		catch (\Exception $e){
			throw $this->createAccessDeniedException();
		}
		$em = $this->getDoctrine()->getManager();
		if(null != $friend_id = $request->request->get('friend')){
			if(null != $friend = $em->getRepository('TintchAPIBundle:AppUser')->find($friend_id)){
				if( null !== $demand = $em->getRepository('TintchAPIBundle:Demande')->findOneBy(array('user'=>$session->getIdUser(), 'friend'=>$friend))){
					try {
						$em->remove($demand);
						$em->flush();
						return FOSView::create(array('code'=>Codes::HTTP_OK,'message'=>'Demand deleted'), Codes::HTTP_OK);
					}catch (\Exception $e){
						return FOSView::create(array('code'=>$e->getCode(),'message'=>$e->getMessage()), $e->getCode());
					}
				}else{
					return FOSView::create(array('code'=>Codes::HTTP_NOT_FOUND,'message'=>'This demand does not exist'), Codes::HTTP_NOT_FOUND);
				}
			}else{
				return FOSView::create(array('code'=>Codes::HTTP_NOT_FOUND,'message'=>'Friend user not found'), Codes::HTTP_NOT_FOUND);
			}
		}else{
			return FOSView::create(array('code'=>Codes::HTTP_BAD_REQUEST,'message'=>'Please define parameter friend'), Codes::HTTP_BAD_REQUEST);
		}
	}
}
