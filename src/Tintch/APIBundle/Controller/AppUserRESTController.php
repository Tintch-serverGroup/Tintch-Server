<?php

namespace Tintch\APIBundle\Controller;

use Tintch\APIBundle\Entity\AppUser;
use Tintch\APIBundle\Form\AppUserCreateType;
use Tintch\APIBundle\Form\AppUserPutType;
use Tintch\APIBundle\Form\AppUserReturnType;

use Tintch\APIBundle\Classes\GeolocalisationManager;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;

use Tintch\APIBundle\Classes\ProfilPictureManager;

/**
 * AppUser controller.
 * @RouteResource("User")
 */
class AppUserRESTController extends VoryxController
{
	use TraitSessionManager;
    /**
     * Get a AppUser entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getUserAction(AppUser $entity)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
    	if($entity->getProfilPicture() != null){
	    	$urlProfilPicture = stream_get_contents($entity->getProfilPicture());
	    	$base64 = base64_encode(file_get_contents($urlProfilPicture));
    	}else{
    		$base64 = null;
    	}
    	$array = array("id"=>$entity->getId() ,"username"=>$entity->getUsername(), "profilPicture"=>$base64);
		$json = json_encode($array, JSON_UNESCAPED_SLASHES);
    	
        $response = new Response();
        $response->setContent($json);
        
    	return $response;
    }
    /**
     * Get a AppUser entity me
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getMeAction()
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
       	$em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('TintchAPIBundle:AppUser')->find($session->getId());
    	if($entity->getProfilPicture() != null){
	    	$urlProfilPicture = stream_get_contents($entity->getProfilPicture());
	    	$base64 = base64_encode(file_get_contents($urlProfilPicture));
    	}else{
    		$base64 = null;
    	}
    	$array = array("id"=>$entity->getId() ,"username"=>$entity->getUsername(), "profilPicture"=>$base64);
		$json = json_encode($array, JSON_UNESCAPED_SLASHES);
    	
        $response = new Response();
        $response->setContent($json);
        
    	return $response;
    }
    /**
     * Get all AppUser entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
        try {
            $offset = $paramFetcher->get('offset');
            $limit = $paramFetcher->get('limit');
            $order_by = $paramFetcher->get('order_by');
            $filters = !is_null($paramFetcher->get('filters')) ? $paramFetcher->get('filters') : array();

            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('TintchAPIBundle:AppUser')->findBy($filters, $order_by, $limit, $offset);
            $tab = array();
            if ($entities) {
            	foreach ($entities as $entity){
            		$gm = new GeolocalisationManager($em, $entity);
            		$check = $gm->checkIfNeedInitGeolocTable();
            		if($check == true){
            			$gm->initGeolocationTable();
            		}
            	}
                return $entities;
            }
            return FOSView::create('Not Found', Codes::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Create a AppUser entity.
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postAction(Request $request)
    {
        $entity = new AppUser();
        $form = $this->createForm(new AppUserCreateType(), $entity, array("method" => $request->getMethod()));
        $this->removeExtraFields($request, $form);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $geolocManager = new GeolocalisationManager($em, $entity);
            $geolocManager->initGeolocationTable();

            return $entity;
        }

        return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
    }
    /**
     * Update a AppUser entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request, AppUser $entity)
    {
    	try {
    		self::checkSession($entity);
    	}
  		catch (\Exception $e){
           	throw $this->createAccessDeniedException();
  		}
        try {
            $em = $this->getDoctrine()->getManager();
            $request->setMethod('PATCH'); //Treat all PUTs as PATCH
            $form = $this->createForm(new AppUserPutType(), $entity, array("method" => $request->getMethod()));
            $this->removeExtraFields($request, $form);
            $form->handleRequest($request);
            if ($form->isValid()) {
            	if(null != $allow = $request->request->get('allow_geolocation')){
            		if($allow == 'true'){
            			$entity->setAllowGeolocation(true);
            		} else if($allow == 'false'){
            			$entity->setAllowGeolocation(false);
            		}
            	}
                $em->flush();

                return $entity;
            }

            return FOSView::create(array('errors' => $form->getErrors()), Codes::HTTP_INTERNAL_SERVER_ERROR);
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Partial Update to a AppUser entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
*/
    public function patchAction(Request $request, AppUser $entity)
    {
        return $this->putAction($request, $entity);
    }
    /**
     * Delete a AppUser entity.
     *
     * @View(statusCode=204)
     *
     * @param Request $request
     * @param $entity
     * @internal param $id
     *
     * @return Response
     */
    public function deleteAction(Request $request, AppUser $entity)
    {
    	try {
    		self::checkSession($entity);
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            return null;
        } catch (\Exception $e) {
            return FOSView::create($e->getMessage(), Codes::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
