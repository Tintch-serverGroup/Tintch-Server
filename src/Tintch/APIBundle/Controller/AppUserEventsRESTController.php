<?php

namespace Tintch\APIBundle\Controller;

use Tintch\APIBundle\Entity\AppUserEvents;
use Tintch\APIBundle\Entity\AppUserGroups;
use Tintch\APIBundle\Entity\AppGroup;
use Tintch\APIBundle\Entity\AppUser;
use Tintch\APIBundle\Entity\AppEvent;

use Tintch\APIBundle\Form\AppUserEventsType;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Voryx\RESTGeneratorBundle\Controller\VoryxController;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * AppUserEvents controller.
 * @RouteResource("UserByEvent")
 */
class AppUserEventsRESTController extends VoryxController
{
	use TraitSessionManager;
    /**
     * Get a AppUserEvents entity
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @return Response
     *
     */
    public function getAction(AppUserEvents $entity)
    {
    }
    /**
     * Get all AppUserEvents entities.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param ParamFetcherInterface $paramFetcher
     *
     * @return Response
     *
     * @QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing notes.")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="How many notes to return.")
     * @QueryParam(name="order_by", nullable=true, array=true, description="Order by fields. Must be an array ie. &order_by[name]=ASC&order_by[description]=DESC")
     * @QueryParam(name="filters", nullable=true, array=true, description="Filter by fields. Must be an array ie. &filters[id]=3")
     */
    public function cgetAction(ParamFetcherInterface $paramFetcher)
    {
    }
    /**
     * Create a AppUserEvents entity.
     *
     * @View(statusCode=201, serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     *
     * @return Response
     *
     */
    public function postUserAction(Request $request)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
        $id_event = $request->request->get('event');
        $id_user = $request->request->get('user');

        $em = $this->getDoctrine()->getManager();

        $event = $em->getRepository('TintchAPIBundle:AppEvent')->find($id_event);

        if($session->getIdUser() != $event->getAdmin()){
			return FOSView::create(array('code'=>Codes::HTTP_FORBIDDEN,'message'=>'Forbidden'), Codes::HTTP_FORBIDDEN);
        }        
        $user = $em->getRepository('TintchAPIBundle:AppUser')->find($id_user);

        if($event != false && $user != false){
        	$entity = new AppUserEvents($event,$user);
        	
        	$em->persist($entity);
			$em->flush();

			return FOSView::create(array('code'=>Codes::HTTP_OK,'message'=>'user '.$user->getUsername().' add to event'), Codes::HTTP_OK);
        }
        
        return FOSView::create(array('code'=>Codes::HTTP_BAD_REQUEST,'message' => 'user or event not correct, or user allready in event'), Codes::HTTP_BAD_REQUEST);
    }
    
    public function postGroupAction(Request $request)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
	        $id_event = $request->request->get('event');
	        $id_group = $request->request->get('group');

		if(null !== $id_event && null !== $id_group){
	        $em = $this->getDoctrine()->getManager();
	
	        $event = $em->getRepository('TintchAPIBundle:AppEvent')->find($id_event);
	        
	        if( $session->getIdUser() != $event->getAdmin()){
				return FOSView::create(array('code'=>Codes::HTTP_FORBIDDEN,'message'=>'Forbidden'), Codes::HTTP_FORBIDDEN);
	        }        
	        $group = $em->getRepository('TintchAPIBundle:AppGroup')->find($id_group);
	        if($event != false && $group != false){
				
	        	$query = $em->createQuery(
	        			'SELECT u
			    FROM TintchAPIBundle:AppUser u
				LEFT JOIN TintchAPIBundle:AppUserGroups ubg
				WITH u.id = ubg.user
			    WHERE ubg.group = :group'
	        	)->setParameter('group', $group);
	        	
	        	$entities = $query->getResult(\Doctrine\ORM\Query::STATE_CLEAN);
	        	
	        	$query2 = $em->createQuery(
	        			'SELECT u
			    FROM TintchAPIBundle:AppUser u
				LEFT JOIN TintchAPIBundle:AppUserEvents ube
				WITH u.id = ube.user
			    WHERE ube.event = :event'
	        	)->setParameter('event', $event);
	
	        	$userbyevent = $query2->getResult(\Doctrine\ORM\Query::STATE_CLEAN);
	        	
	        	$al = new ArrayCollection((array)$userbyevent);
	        	
	        	foreach ($entities as $user){
	        		try {
	        			if(!$al->contains($user)){
							$entity = new AppUserEvents($event, $user);
							$em->persist($entity);
							$em->flush();
	        			}
					}catch (\Exception $e){
						return FOSView::create(array('$code'=>$e->getCode(),'message'=>$e->getMessage()), Codes::HTTP_INTERNAL_SERVER_ERROR);
					}
	        	}
        	}
			return FOSView::create(array('code'=>Codes::HTTP_OK,'message'=>'group add to event'), Codes::HTTP_OK);
			
        }

        return FOSView::create(array('code'=>Codes::HTTP_BAD_REQUEST,'message' => 'group or event not correct, or user allready in event'), Codes::HTTP_BAD_REQUEST);
    }
    /**
     * Update a AppUserEvents entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
     */
    public function putAction(Request $request, AppUserEvents $entity)
    {
    }
    /**
     * Partial Update to a AppUserEvents entity.
     *
     * @View(serializerEnableMaxDepthChecks=true)
     *
     * @param Request $request
     * @param $entity
     *
     * @return Response
*/
    public function patchAction(Request $request, AppUserEvents $entity)
    {
    }
    /**
     * Delete a AppUserEvents entity.
     *
     * @return Response
     */
    public function deleteAction(Request $request)
    {
    	try {
    		$session = self::checkSession();
    	}
    	catch (\Exception $e){
    		throw $this->createAccessDeniedException();
    	}
        $id_event = $request->request->get('event');
        $id_user = $request->request->get('user');
		if(null !== $id_event && null !== $id_user){
	        $em = $this->getDoctrine()->getManager();
			$userevent = $em->getRepository('TintchAPIBundle:AppUserEvents')->findOneBy(array('event' => $id_event, 'user' => $id_user));
			if($userevent != false){
		        if($session->getIdUser() != $userevent->getEvent()->getAdmin()){
					return FOSView::create(array('code'=>Codes::HTTP_FORBIDDEN,'message'=>'Forbidden'), Codes::HTTP_FORBIDDEN);
		        }
				try {
			        $em->remove($userevent);
			        $em->flush();
					return FOSView::create(array('code'=>Codes::HTTP_OK,'message'=>'user remove to event'), Codes::HTTP_OK);
				}catch (\Exception $e){
					return FOSView::create(array('code'=>Codes::HTTP_INTERNAL_SERVER_ERROR,'message'=> $e->getMessage()), Codes::HTTP_INTERNAL_SERVER_ERROR);
				}
			}
		}
        return FOSView::create(array('errors' => 'user or event not correct'), Codes::HTTP_BAD_REQUEST);
    }
}
