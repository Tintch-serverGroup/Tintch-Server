<?php

namespace Tintch\APIBundle\Classes;

use Tintch\APIBundle\Entity\AppUser;
use Tintch\APIBundle\Entity\GeolocUser;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View as FOSView;
use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Types\IntegerType;

class GeolocalisationManager{
	const numberOfEntry = 20;
	private $em;
	private $user;
	private $current_index_location;
	private $next_index_location;
	
	public function __construct(EntityManager $em, AppUser $user){
		$this->em = $em;
		$this->user = $user;
		$this->current_index_location = $this->user->getIndexGeolocation();
		if($this->current_index_location < self::numberOfEntry-1){
			$this->next_index_location = $this->current_index_location + 1;
		}else {
			$this->next_index_location = 0;
		}
	}
	public function checkIfNeedInitGeolocTable(){
		try {
			$query = $this->em->createQuery(
	        			'SELECT count(u)
			    FROM TintchAPIBundle:GeolocUser u
			    WHERE u.user = :user'
	        	)->setParameter('user', $this->user);
		
			$result = $query->getSingleScalarResult();
			
			if($result == 0){
				return true;
			} else if($result == self::numberOfEntry){
				return false;
			} else {
				$query = $this->em->createQuery(
						'SELECT u
				    FROM TintchAPIBundle:GeolocUser u
				    WHERE u.user = :user'
				)->setParameter('user', $this->user);
				$result = $query->getResult();
				foreach ($result as $entry){
					$this->em->remove($entry);
				}
				$this->em->flush();
				return true;
			}
		} catch (\Exception $e){
			throw $e;
		}
	}	
	
	public function initGeolocationTable(){
		try {
			for($i=0;$i<self::numberOfEntry;$i++){
				$entity = new GeolocUser();
				$entity->setId($i);
				$entity->setUser($this->user);
				$this->em->persist($entity);
			}
			$this->em->flush();
			
			return FOSView::create(array('code' => Codes::HTTP_CREATED, 'message' => 'geolocation create for this user'), Codes::HTTP_CREATED);
		} catch (\Exception $e){
			throw $e;
		}
	}
	
	public function getLastLocation(){
		try {
			$location = $this->em->getRepository('TintchAPIBundle:GeolocUser')->findOneBy(array('id'=>$this->current_index_location,'user'=>$this->user));

			$tab = array();
			$tab[] = array(
					'id' => $location->getId(),
					'date' => $location->getDate(),
					'user' => array('id'=>$location->getUser()->getId(), 'username'=>$location->getUser()->getUsername()),
					'latitude' => $location->getLatitude(),
					'longitude' => $location->getLongitude()
			);
			$tab = json_encode($tab, JSON_UNESCAPED_SLASHES);
			return $tab;
		} catch (\Exception $e){
			throw $e;
		}
	}
	
	public function getLastFiveLocation(){
		try {
			$query = $this->em->createQuery(
					'SELECT u
				    FROM TintchAPIBundle:GeolocUser u
				    WHERE u.user = :user
					ORDER BY u.date DESC'
			)->setParameter('user', $this->user);
			$query->setMaxResults(5);
			
			$result = $query->getResult();
			$locations = array();
			foreach ($result as $location){
				$locations[] = array(
					'id' => $location->getId(),
					'date' => $location->getDate(),
					'user' => array('id'=>$location->getUser()->getId(), 'username'=>$location->getUser()->getUsername()),
					'latitude' => $location->getLatitude(),
					'longitude' => $location->getLongitude()	
				);
			}
			$locations = json_encode($locations, JSON_UNESCAPED_SLASHES);
			return $locations;
		} catch (\Exception $e){
			throw $e;
		}
	}
	
	public function getLastTenLocation(){
		try {
			$query = $this->em->createQuery(
					'SELECT u
				    FROM TintchAPIBundle:GeolocUser u
				    WHERE u.user = :user
					ORDER BY u.date DESC'
			)->setParameter('user', $this->user);
			$query->setMaxResults(10);
			
			$result = $query->getResult();
			$locations = array();
			foreach ($result as $location){
				$locations[] = array(
					'id' => $location->getId(),
					'date' => $location->getDate(),
					'user' => array('id'=>$location->getUser()->getId(), 'username'=>$location->getUser()->getUsername()),
					'latitude' => $location->getLatitude(),
					'longitude' => $location->getLongitude()	
				);
			}
			$locations = json_encode($locations, JSON_UNESCAPED_SLASHES);
			return $locations;
		} catch (\Exception $e){
			throw $e;
		}
	}
	
	public function getLastTwentyLocation(){
		try {
			$query = $this->em->createQuery(
					'SELECT u
				    FROM TintchAPIBundle:GeolocUser u
				    WHERE u.user = :user
					ORDER BY u.date DESC'
			)->setParameter('user', $this->user);
			$query->setMaxResults(20);
			
			$result = $query->getResult();
			$locations = array();
			foreach ($result as $location){
				$locations[] = array(
					'id' => $location->getId(),
					'date' => $location->getDate(),
					'user' => array('id'=>$location->getUser()->getId(), 'username'=>$location->getUser()->getUsername()),
					'latitude' => $location->getLatitude(),
					'longitude' => $location->getLongitude()	
				);
			}
			$locations = json_encode($locations, JSON_UNESCAPED_SLASHES);
			return $locations;
		} catch (\Exception $e){
			throw $e;
		}
	}
	
	public function setLocation($latitude, $longitude){
		try {
			$geolocUser = $this->em->getRepository('TintchAPIBundle:GeolocUser')->findOneBy(array('id'=>$this->next_index_location, 'user'=>$this->user));
			$date = new \DateTime('NOW');
			$date->format(\DateTime::W3C);
			$geolocUser->setDate($date);
			$geolocUser->setLatitude($latitude);
			$geolocUser->setLongitude($longitude);
			$this->user->setIndexGeolocation($this->next_index_location);
				
			$this->em->persist($geolocUser);
			$this->em->persist($this->user);
			$this->em->flush($geolocUser);
			$this->em->flush($this->user);
			
			return FOSView::create(array('code' => Codes::HTTP_CREATED, 'message' => 'geolocation create for this user'), Codes::HTTP_CREATED);
		} catch (\Exception $e){
			throw $e;
		}
	}
}
