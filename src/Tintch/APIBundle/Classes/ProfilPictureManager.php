<?php

namespace Tintch\APIBundle\Classes;

use \Exception;
use FOS\RestBundle\Util\Codes;

class ProfilPictureManager
{
	private $target_file;
	private $target_file_icon;
	private $file;
	private $target_dir_original;
	private $target_dir_icon;
	
	public function __construct($user, $file = null){
		//Check if folder exist on server
		$image_dir = dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR .'pictures';
		if(!is_dir($image_dir)){
			mkdir($image_dir);
		}
		$users_dir = $image_dir.DIRECTORY_SEPARATOR .'users';
		if(!is_dir($users_dir)){
			mkdir($users_dir);
		}
		$user_dir = $users_dir.DIRECTORY_SEPARATOR .$user;
    		if(!is_dir($user_dir)){
			mkdir($user_dir);
		}
		$original = $user_dir.DIRECTORY_SEPARATOR .'original';
    		if(!is_dir($original)){
			mkdir($original);
		}
		$this->target_dir_original = $original.DIRECTORY_SEPARATOR;
		$icon = $user_dir.DIRECTORY_SEPARATOR .'icon';
    		if(!is_dir($icon)){
			mkdir($icon);
		}
		$this->target_dir_icon = $icon.DIRECTORY_SEPARATOR;
		
		if($file != null){
			$this->file = $file;
			$this->target_file = $this->target_dir_original.basename($this->file['name']);
			$this->target_file_icon = $this->target_dir_icon.basename($this->file['name']);
		}
	}
	
	public function getOriginalPicture($filename){
		$tab = array();
		if(is_file($this->target_dir_original.DIRECTORY_SEPARATOR.$filename)){
			$tab[basename($this->target_dir_original.DIRECTORY_SEPARATOR.$filename)] = base64_encode(file_get_contents($this->target_dir_original.DIRECTORY_SEPARATOR.$filename));
			return $tab;
		}else{
			throw new \Exception('File not found',Codes::HTTP_NOT_FOUND);
		}
	}
	
	public function uploadPicture(){
		$imageFileType = pathinfo($this->target_file,PATHINFO_EXTENSION);
		$check = getimagesize($this->file["tmp_name"]);
		if($check !== false) {
			// 		        echo "File is an image - " . $check["mime"] . "."
		} else {
			throw new \Exception('File is not an image', Codes::HTTP_NOT_ACCEPTABLE);
		}
		// Check if file already exists
		if (file_exists($this->target_file)) {
			throw new \Exception('File already exists', Codes::HTTP_NOT_ACCEPTABLE);
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
			throw new \Exception('Only JPG, JPEG, PNG & GIF files are allowed', Codes::HTTP_NOT_ACCEPTABLE);
		}
		$icone = $this->RedimensionnerImage($this->file["tmp_name"], 200, 200);
		if (move_uploaded_file($this->file["tmp_name"], $this->target_file) && imagejpeg($icone,$this->target_file_icon)){
			imagedestroy($icone);
		return $this->target_file_icon;
		} else {
			throw new \Exception('There was an error uploading your file', Codes::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
	
	public function getAllProfilPictureIcon(){
		try {
			$tab = array();
			if($folder = opendir($this->target_dir_icon)){
				while(false !== ($file = readdir($folder))){
					if($file != '.' && $file != '..')
					{
						$base64 = base64_encode(file_get_contents($this->target_dir_icon.DIRECTORY_SEPARATOR.$file));
						$tab[$file] = $base64;
					}
				}
			}
		}catch (\Exception $e){
			throw $e;
		}
		return $tab;
	}
	
	public function deletePicture($filename){
		try {
			$file = $this->target_dir_original.DIRECTORY_SEPARATOR.$filename;
			$icon = $this->target_dir_icon.DIRECTORY_SEPARATOR.$filename;
			if (file_exists($file) && file_exists($icon)) {
				unlink($file);
				unlink($icon);
			}
		}catch (\Exception $e){
			
		}
	}
	
	private function RedimensionnerImage($source, $new_value_largeur, $new_value_hauteur) {
		if( !( list($source_largeur, $source_hauteur) = @getimagesize($source) ) ) {
			return false;
		}
		$nouv_largeur = $new_value_largeur;
		$nouv_hauteur = $new_value_hauteur;
		
		$image = imagecreatetruecolor($nouv_largeur, $nouv_hauteur);
		$source_image = imagecreatefromstring(file_get_contents($source));
		imagecopyresampled($image, $source_image, 0, 0, 0, 0, $nouv_largeur, $nouv_hauteur, $source_largeur, $source_hauteur);
		imagedestroy($source_image);
		
		return $image;
	}
}