<?php

namespace Tintch\APIBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AppUserPutType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email','email',
                    array(
                        'required'  =>
                            false                       
                        ))
            ->add('username','text',
                    array(
                        'required'  =>
                            false                       
                        ))
            ->add('password','password',
                    array(
                        'required'  =>
                            false                       
                        ))
            ->add('allow_geolocation','checkbox',
                    array(
                        'required'  =>
                            false                       
                        ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tintch\APIBundle\Entity\AppUser'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tintch_apibundle_appuser';
    }
}
