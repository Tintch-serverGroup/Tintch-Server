<?php

namespace Tintch\APIBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventPutType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name','text',
                    array(
                        'required'  =>
                            false                       
                        ))
            ->add('longitude','number',
                    array(
                        'required'  =>
                            false                       
                        ))
            ->add('latitude','number',
                    array(
                        'required'  =>
                            false                       
                        ))
            ->add('description','text',
                    array(
                        'required'  =>
                            false                       
                        ))
            ->add('picture','text',
                    array(
                        'required'  =>
                            false                       
                        ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Tintch\APIBundle\Entity\AppEvent'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tintch_apibundle_appevent';
    }
}
